%*****************************************
%Fish tracking
%Author: Andreas Nold, December 11th, 2015
%
%Tested with Matlab R2014b
%*****************************************
%Working Examples:
% file                     time interval (sec.)
%Video01_09Nov2015.mp4       0..2
%Video02_09Nov2015.mp4       0..1
%Video04_09Nov2015.mp4       12..14
%Video05_09Nov2015.mp4       24..26

%Initialize
addpath(genpath(pwd));        

% Get filename
disp('Select Video file to analyse');
[FileName,DataFolder] = uigetfile('*.mp4','Select Video file to analyse');            
filename = [DataFolder,FileName];

disp(['Loading ',FileName,'..']);
tInt(1) = input('Start analysis at time t_start(sec.) = ');
tInt(2) = input('End analysis at time t_end(sec.) = ');            

%Do Analysis
[xyt,bounds] = AnalyseVideo(filename,tInt);    

%Play Video
PlayVideoWithCoordinates(filename,xyt,bounds);
