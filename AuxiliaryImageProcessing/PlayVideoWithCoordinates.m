function PlayVideoWithCoordinates(filename,xyt,bounds)

    vid_  = VideoReader(filename);            
    
    for k = 1:size(xyt,1)
        vid_.CurrentTime = xyt(k,3);
        frame = readFrame(vid_);    
        frame = CropNonDomain(frame,bounds);
        frame = PutRedDotAtPosition(frame,xyt(1:k,1:2));

        imshow(frame);
%         disp(['Coordinates (x,y) = (',num2str(xyt(k,1)),',',...
%                                       num2str(xyt(k,2)),')',...
%                        'at time t = ',num2str(xyt(k,3))]);
        pause(0.1);
    end
end