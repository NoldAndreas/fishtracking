function filter = FindFishPixels(frame,opts)
    if(IsOption(opts,'whiteArena'))
        filter  = (FindExtremesColors(GetRedness(frame),150,'bright'));
    elseif(IsOption(opts,'blackArena'))
        filter  = (FindExtremesColors(GetNegBlueness(frame),60,'bright')); 
    end    
    filter = RemoveSmallClusters(filter);
end