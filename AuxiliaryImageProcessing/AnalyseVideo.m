function [xyt,bounds,Lblack] = AnalyseVideo(filename,tInt)

    %********************************************
    %0. Initialize
    vid_  = VideoReader(filename);        

    %********************************************
    %1. Find arena   
    [frame,bounds,Lblack] = FindArena();    
    
    %********************************************
    %2. Go through each frame and estimate position of 
    %   fish via color analysis of image
    k = 1;
    vid_.CurrentTime = tInt(1);
    h = waitbar(0,'Analysing ..');
    while (vid_.CurrentTime < tInt(2))                
        frame = readFrame(vid_);    
    
        [~,xy] = (GetFishPosition(frame,bounds));
        xyt(k,1) = xy(1);
        xyt(k,2) = xy(2); 
        xyt(k,3) = vid_.CurrentTime;
        waitbar((xyt(k,3)-tInt(1))/(tInt(2)-tInt(1)));
        k    = k + 1;                        
    end
    close(h);       
    
    %********************************************
    %3. Postprocess results
    [xyt,vel]  = PostProcessVideo(filename,xyt,bounds,Lblack);
end