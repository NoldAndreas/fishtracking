function frame = GetGrayFrame(frame)
    frame = double(frame(:,:,1))/3 + double(frame(:,:,2))/3 + double(frame(:,:,3))/3;
   % frame = uint8(frame);
end