function filter = FilterDistance(frame,xy,dist)
%Sets all pixels to true that are at distance dist 
% or smaller of point defined by xy  
    N  = sum(sum(frame));
    v1 = (1:size(frame,1));
    v2 = (1:size(frame,2));
    
    e1 = ones(size(frame,1),1);
    e2 = ones(1,size(frame,2));
    

    D      = sqrt( (v1'*e2 - xy(1)).^2 + (e1*v2 - xy(2)).^2);
    filter = (D <= dist);    
end