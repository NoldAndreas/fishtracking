function [frame,xy,sigma] = GetFishPosition(frame,bounds)
    frame     = CropNonDomain(frame,bounds);
    
    [frameBlack,frameWhite] = GetBlackAndWhiteArena(frame);            
    Lblack                  = size(frameBlack,2);
    frameBlack              = frameBlack(:,1:end-4,:);
    
    %*******************************************
    %Find 'fish-pixels' in black part of domain
    blackArenaFilter        = FindFishPixels(frameBlack,{'blackArena'}); 
    [xyBlack,sigmaBlack]    = GetAveragePosition(blackArenaFilter);    
    
    %*******************************************
    %Find 'fish-pixels' in white part of domain    
    whiteArenaFilter     = FindFishPixels(frameWhite,'whiteArena');
    [xyWhite,sigmaWhite] = GetAveragePosition(whiteArenaFilter);
    xyWhite(2)           = xyWhite(2) + Lblack;
       
    %*******************************************
    %Decide where fish is located
    % Indicators: 
    % (1) Sum of pixels (if not enough pixels, then discard)
    % (2) Standard deviation (if pixels are too located, then filter didn't work)
    
    if((norm(sigmaWhite) < 70) && (norm(sigmaBlack) > 100))
        xy = xyWhite; sigma  = sigmaWhite;
    elseif((norm(sigmaWhite) > 100) && (norm(sigmaBlack) < 70))
        xy = xyBlack; sigma  = sigmaBlack;
    else
        blackArenaFilter     = FindFishPixels(frameBlack,{'blackArena'}); 
        [xyBlack,sigmaBlack] = GetAveragePosition(blackArenaFilter);            
    
        if((norm(sigmaWhite) < 70) && (norm(sigmaBlack) > 100))
            xy = xyWhite; sigma = sigmaWhite;
        elseif((norm(sigmaWhite) > 100) && (norm(sigmaBlack) < 70))
            xy = xyBlack; sigma = sigmaBlack;
        else        
            if(sum(sum(blackArenaFilter)) > sum(sum(whiteArenaFilter)))
                xy        = xyBlack;
            else
                xy        = xyWhite;
            end
        end
    end       
end