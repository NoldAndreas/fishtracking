function frame = PutRedDotAtPosition(frame,xy)
    d = -2:2;
    
    for i = 1:size(xy,1)
        X = max(round(xy(i,1))+d,1);
        Y = max(round(xy(i,2))+d,1);
        frame(X,Y,1) = 255;        
        frame(X,Y,2) = 0;
        frame(X,Y,3) = 0;
    end
end