function frameRedF = GetRedness(frame)    
    frameRed = double(frame(:,:,1)) -double(GetGrayFrame(frame));
    frameRed = frameRed*255/max(max(frameRed));
    frameRedF(:,:,1) = frameRed;
    frameRedF(:,:,2) = frameRed;
    frameRedF(:,:,3) = frameRed;
end