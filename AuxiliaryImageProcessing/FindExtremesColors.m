function bool = FindExtremesColors(frame,brightness,opts)

    avCol = AverageColor(frame);
    
    if(IsOption(opts,'bright'))
        bool = ((frame(:,:,1) > avCol(1) + brightness) & ...
                (frame(:,:,2) > avCol(2) +  brightness) & ...
                (frame(:,:,3) > avCol(3) +  brightness));    
    elseif(IsOption(opts,'dark'))
        bool = ((frame(:,:,1) < avCol(1) + brightness) & ...
            (frame(:,:,2) < avCol(1) + brightness) & ...
            (frame(:,:,3) < avCol(1) + brightness));            
    end    
end
