  function [frameBlack,frameWhite] = GetBlackAndWhiteArena(frame)
    %Go from left to right, identify point where next 5 consecutive points
    % have an incrase in brightness over 130 in all colors
    line = frame(floor(end/2),:,:);
    n = 5;

    for i = 1:size(line,2)
        if(sum(sum(line(1,(i:i+n-1),:) > 130)) == n*3)
            break;
        end
    end
    blackWhiteBound = i;

    frameBlack = frame(:,1:blackWhiteBound-1,:);
    frameWhite = frame(:,blackWhiteBound:end,:);
end