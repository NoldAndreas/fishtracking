function [vel,vel_norm] = GetVelocities(xyt)
    %Return velocity in pixels per time step
    
    vel          = zeros(size(xyt,1),2);
    vel(2:end,1) = (xyt(2:end,1) - xyt(1:end-1,1));
    vel(2:end,2) = (xyt(2:end,2) - xyt(1:end-1,2));        
    vel_norm     = sqrt( (vel(:,1)).^2 + (vel(:,2)).^2);
end