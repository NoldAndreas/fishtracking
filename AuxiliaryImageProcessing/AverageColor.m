function avCol = AverageColor(frame)
    N     = size(frame,1)*size(frame,2);
    avCol = (sum(sum(frame)))/N;
    avCol = permute(avCol,[3 2 1]);
end