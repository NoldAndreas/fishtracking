function frameRedF = GetNegBlueness(frame)    
    %frameRed = double(GetGrayFrame(frame)) - double(frame(:,:,3)) ;
    frameRed = max(double(frame(:,:,1)),double(frame(:,:,2)))  - double(frame(:,:,3));
    frameRed = frameRed*255/max(max(frameRed));
    frameRedF(:,:,1) = frameRed;
    frameRedF(:,:,2) = frameRed;
    frameRedF(:,:,3) = frameRed;
end