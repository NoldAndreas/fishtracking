function frame = CropNonDomain(frame,bounds)        
    frame = frame(bounds(4):bounds(2),bounds(1):bounds(3),:);       
end