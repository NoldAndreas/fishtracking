function [xy,sigma] = FindFishXY(frame,xy_guess)

    [frameBlack,frameWhite] = GetBlackAndWhiteArena(frame);            
    frameBlack = frameBlack(:,1:end-4,:);
    
    %Find 'fish-pixels' in black part of domain
    blackArenaFilter     = FindFishPixels(frameBlack,{'blackArena'});
    
    %Find 'fish-pixels' in white part of domain
    whiteArenaFilter     = FindFishPixels(frame,'whiteArena');    
    
    %Filter to only see pixels that are 100 pixels far away from xy_guess
    filter               = FilterDistance(frame,xy_guess,120);
    blackArenaFilter     = blackArenaFilter & filter(:,1:size(blackArenaFilter,2));
    whiteArenaFilter     = whiteArenaFilter & filter;
    
    [xyWhite,sigmaWhite] = GetAveragePosition(whiteArenaFilter);
    [xyBlack,sigmaBlack] = GetAveragePosition(blackArenaFilter);    
    sumWhite = sum(sum(whiteArenaFilter));
    sumBlack = sum(sum(blackArenaFilter));
        
    if((norm(sigmaWhite) < 70) && ((norm(sigmaBlack) > 100) || (sumBlack < 100)))
        xy = xyWhite; sigma = sigmaWhite;
    elseif((norm(sigmaWhite) > 100) && ( (norm(sigmaBlack) < 70) || (sumWhite < 100)))
        xy = xyBlack; sigma = sigmaBlack;
    else        
        if(max(sumWhite,sumBlack) == 0)
            xy = [0,0]; sigma = [0,0];
        elseif(sumBlack > sumWhite)
            xy        = xyBlack; sigma = sigmaBlack;
        else
            xy        = xyWhite; sigma = sigmaWhite;
        end
    end
end
