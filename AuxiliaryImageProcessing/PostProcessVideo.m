function [xyt,vel,vel_norm,times] = PostProcessVideo(filename,xyt,bounds,Lblack)

    %************************************************
    %Parameters
    maxVel = 60; %max accepted velicity of fish as pixels per time step
    %************************************************
        
    %************************************************
    %1. Identify times with erroneously large velocities    
    [vel,vel_norm] = GetVelocities(xyt);
    acceptedPoints = ( (vel_norm < maxVel));% & ([0;vel_norm(2:end)] < maxVel));
    xyt(~acceptedPoints,1:2) = 0;
    
    %************************************************    
    %2. Go through points that were not accepted and search in the vicinity
    %   of the last point, if it was accepted
    vid_  = VideoReader(filename);            
    kmax = size(xyt,1);
    for k = 2:kmax
        if(acceptedPoints(k))    
            continue;
        end
        disp(['Postprocessing frame k = ',num2str(k)]);        
        vid_.CurrentTime = xyt(k,3);
        frame            = readFrame(vid_);    
        frame            = CropNonDomain(frame,bounds);
        
        if(acceptedPoints(k-1))
            xyt(k,1:2)  = FindFishXY(frame,xyt(k-1,1:2));
        elseif((k <= kmax -1) && acceptedPoints(k+1))
            xyt(k,1:2)  = FindFishXY(frame,xyt(k+1,1:2));            
        else                     
            continue;            
        end
    end
    
    %************************************************
    %3. Identify times with erroneously large velocities        
    [vel,vel_norm] = GetVelocities(xyt);
    acceptedPoints = ( (vel_norm < maxVel) & ([0;vel_norm(2:end)] < maxVel));    
    xyt(~acceptedPoints,1:2) = 0;
    disp(['No of not accepted pts: ',num2str(sum(~acceptedPoints))]);
            
    %************************************************
    %4. Get time spent in black arena:
    deltaT = [(xyt(2,3) - xyt(1,3))/2;...
              (xyt(3:end,3) - xyt(1:end-2,3))/2;...
              (xyt(end,3)-xyt(end-1,3))/2];
        
    times.tBlack = sum(((xyt(:,1) <  Lblack) & acceptedPoints).*deltaT);
    times.tWhite = sum(((xyt(:,1) >= Lblack) & acceptedPoints).*deltaT);
    times.tundefined = sum((~acceptedPoints).*deltaT);
    T   = (xyt(end,3) - xyt(1,3));
    error = times.tBlack + times.tWhite + times.tundefined - T;
    
    %************************************************
    %5. Display results
    disp(['Time in black arena: .',num2str(times.tBlack),' sec ( ',num2str(100*times.tBlack/T),'%)']);
    disp(['Time in white arena: .',num2str(times.tWhite),' sec ( ',num2str(100*times.tWhite/T),'%)']);
    disp(['Time undefined: .',num2str(times.tundefined),' sec ( ',num2str(100*times.tundefined/T),'%)']);
    disp(['Error : ',num2str(error),' sec']);
    
end