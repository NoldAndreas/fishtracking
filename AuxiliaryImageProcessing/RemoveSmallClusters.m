function frameFilter = RemoveSmallClusters(frame)
    %Blank out single black pixels
    e    = ones(size(frame,2)-1,1);
    
    RS   = diag(e,1);  RS(1,1) = 1;%red shift     
    LS   = diag(e,-1); LS(end,end) = 1;%left shift
    
    US   = diag(e,1);  RS(end,end) = 1; %up shift         
    DS   = diag(e,-1); LS(1,1) = 1; %down shift
    
    frame = sparse(frame);
    RS = sparse(RS);
    LS = sparse(LS);
    US = sparse(US);
    DS = sparse(DS);
    
    %for 2 connected pixels:
%    frameFilter = (frame & ((frame*RS) | (frame*LS)));    
    
    %for 3 connected pixels:
    frameFilterLR = (frame & ( ((frame*RS) & (frame*RS*RS)) | ...
                            ((frame*LS) & (frame*RS)) | ...
                            ((frame*LS) & (frame*LS*LS))));
                        
    %for 3 connected pixels:
    frameFilterTD = (frame & ( ((frame*US) & (frame*US*US)) | ...
                            ((frame*US) & (frame*DS)) | ...
                            ((frame*DS) & (frame*DS*DS))));
         
	frameFilter = frameFilterLR & frameFilterTD;
end