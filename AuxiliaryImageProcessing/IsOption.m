function bool = IsOption(opt,field)
    if(~iscell(opt))
        opt = {opt};
    end
    bool = ~isempty(find(ismember(opt,field)));
end