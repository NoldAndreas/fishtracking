function [xy,sigma] = GetAveragePosition(frame)
    %returns average and variance

    N  = sum(sum(frame));
    if(N == 0)
        xy = [0,0];
    else
        sigma = [0,0];
    end
    v1 = (1:size(frame,1));
    v2 = (1:size(frame,2))';

    xy(1) = sum(v1*frame)/N;
    xy(2) = sum(frame*v2)/N;
    
    %Get Variance
    M = frame.*(diag(v1)*frame-xy(1));
    sigma(1) = sqrt(sum(sum(M.^2/N)));
    
	M        = frame.*(frame*diag(v2)-xy(2));
    sigma(2) = sqrt(sum(sum(M.^2/N)));
end