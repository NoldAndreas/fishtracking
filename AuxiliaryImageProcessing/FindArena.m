function [frame,bounds,Lblack] = FindArena()
        
    %Step0: Initialize
    disp('Select location of Video01_09Nov2015.mp4');
    [FileName,DataFolder] = uigetfile('*.mp4','Location of Video01_09Nov2015.mp4');      
    file_staticFishOnBlack = [DataFolder,FileName];    
    vid_  = VideoReader(file_staticFishOnBlack);
    frame = readFrame(vid_);        
    
    %Step1: Cut the outer parts of the box
    bounds1 = [180,630,1150,70];
    frame = CropNonDomain(frame,bounds1);
     
    %Step2: Find one point on each boundary (left,bottom,right,left)    
    [bBottom,bTop] = FindBottomTopBoundary(frame);        
    [bound_Left,bound_Right] = FindLeftRightBoundary(frame);
    
    %Step3: Cut image according to new found boundaries
    bounds2 = [bound_Left,bBottom,bound_Right,bTop];
    frame = CropNonDomain(frame,bounds2);
    
    bounds = [bounds2(1) + bounds1(1),...
              bounds2(2) + bounds1(4),...
              bounds2(3) + bounds1(1),...
              bounds2(4) + bounds1(4)];        
        
	frameBlack = GetBlackAndWhiteArena(frame);    
    Lblack     = size(frameBlack,2);

    function [bBottom,bTop] = FindBottomTopBoundary(frame)        
        
        %start from top boundary, move bottom until hitting 5 consecutive
        % points with values less than 50 in every color      
        line = frame(:,200,:);
        n = 5;
        
        for i = 1:(size(line,1)-n)
            if(sum(sum(line((i:i+n-1),1,:) < 50)) == n*3)
                break;
            end
        end   
        bTop = i;
        
        %start from bottom boundary, move top until hitting 5 consecutive
        % points with values less than 50 in every color      
        for i = size(line,1):-1:n
            if(sum(sum(line((i:-1:i-n+1),1,:) < 50)) == n*3)
                break;
            end
        end   
        bBottom = i;   
    end

    function [bLeft,bRight] = FindLeftRightBoundary(frame)        
        
        %start from left boundary, move right until hitting 5 consecutive
        % points with values less than 50 in every color      
        line = frame(floor(end/2),:,:);
        n = 5;
        
        for i = 1:(size(line,2)-n)
            if(sum(sum(line(1,(i:i+n-1),:) < 50)) == n*3)
                break;
            end
        end   
        bLeft = i;
        
        %start from right boundary, move left until hitting 5 consecutive
        % points with values less than 50 in every color      
        for i = size(line,2):-1:n
            if(sum(sum(line(1,(i:-1:i-n+1),:) < 170)) == n*3)
                break;
            end
        end   
        bRight = i;
    end
        
end
